# 作业要求

## Definition of Done

* 必须非常严格的按照题目中的定义进行实现。
* 程序必须能够在真实 HTTP Client 下工作。
* 必须有单元或者集成测试。每一个测试要代表一个 Task，每一个测试通过之后需要进行一次 commit（小步提交）。
* 测试至少要覆盖业务要求提到的问题。
* 注意代码的坏味道。尤其是命名的规范性以及重复代码的问题。

## 业务要求

又一个面对儿童的产品。迷宫生成器已经发布了第一个版本。但是这个版本没有针对异常情况进行任何处理。因此我们希望对异常进行一些处理。

在进入处理异常的 Story 之前有必要介绍一下目前的 API 的定义……才怪。因为你们都是天才，所以你们自己读读代码估计就知道这个 API 该怎么用了，发挥你们的聪明才智吧少年！

> **注意！！！**
>
> 在完成以下需求的过程之中不得修改现有的任何代码。但是你可以添加新的文件。

## Story 1 返回有效的异常信息（需要有测试）

作为一个用户我希望在出现错误的时候能够给我返回有效的错误消息，而不是出现错误的图片数据。

**AC 1 有效的参数异常信息**

在访问唯一的 API 时，若出现未处理的 `IllegalArgumentException`，则其 Response 如下：

|项目|说明|
|---|---|
|Status Code|400|
|Content-Type|application/json|

其 Body 为

```json
{
  "message": "{message in exception}"
}
```

## Story 2 有效的日志记录（可以无需测试）

作为一个维护人员，我希望在用户出现错误的时候我也能够在后台看到相应的日志记录。以便我来确定这个错误的发生原因。

SpringBoot 里面直接集成了 Slf4J（Simple Logging Facade for Java）。我已经帮助大家配置好了日志记录的各种配置。其实也比较简单。我只是添加了一个文件：*logback.xml*，关于这个配置文件如果你感兴趣的话可以看这两篇（低优先级，不看也没有任何问题）。

* https://www.baeldung.com/spring-boot-logging (Logging in SpringBoot. 特别是 Section 2 和 Section 4)
* https://logback.qos.ch/manual/configuration.html (Configuring Logback)

我在配置文件里特意添加了一些色彩，这样日志看起来也非常 “美丽”（你可以按照自己的喜好配置其他的颜色）。由于我们穷的买不起硬盘。所以我们先把日志显示在 Console 里面。我会让维护人员三班倒盯着屏幕看的（不要去这家公司）。

<img src="./log-example.png"/>

现在我们可以开始做接下来的练习了。

**AC 1 能够在出现未处理的 `IllegalArgumentException` 时记录日志**

* 请在出现未处理的 `IllegalArgumentException` 时记录 *ERROR* 级别的日志。
* 请记录你认为 *有效* 的信息。你认为应该记录什么信息呢？

如果你不太清楚如何记录日志，请看这一篇：https://dzone.com/articles/logging-with-slf4j (中的第一个例子)。为了大家的睡眠，友情提示：

* Java 里面有很多的类都叫做 `Logger` 请确认你使用的是 slf4j 的 `Logger`。
* Logger 的声明可以是静态的，而且一般来说也推荐采用这种方式。

当这个 AC 完成之后当有未处理的 `IllegalArgumentException` 出现的时候，我们就可以从命令行中看到我们输出的日志啦！